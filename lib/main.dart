import 'package:flutter/material.dart';
import 'package:proyecto_flutter_prueba/paginas/Primera_Pagina.dart';

void main(){
  runApp(PruebaFlutter());
}

class PruebaFlutter extends StatelessWidget {  //Widget de tipo Stateless (sin estado)
  @override  //indicamos que vamos a sobreescribir el metodo
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner:  false, //para borrar la etiqueta de DEBUG en el emulador
      title: 'Prueba de Flutter',
      home: PrimeraPagina(), //importamos la pagina 

    );

  }

}