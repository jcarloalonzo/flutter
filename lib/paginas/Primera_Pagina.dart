import 'package:flutter/material.dart';
import 'package:proyecto_flutter_prueba/paginas/Segunda_Pagina.dart';

class PrimeraPagina extends StatefulWidget {
  @override
  _PrimeraPaginaState createState() => _PrimeraPaginaState();
}

class _PrimeraPaginaState extends State<PrimeraPagina> {
  final _formKey = GlobalKey<FormState>(); //Para la validacion de los inputtext

  final controladorIdUsuario =
      TextEditingController(); //para escuchar lo que el usuario a escrito
  final controladorPasswordUsuario = TextEditingController();

  final controladorText = TextEditingController();

  String campoUsuario = '';

  String campoPassword = '';
  bool verifica = false;
  String msj = '';

  void ValidarUsuario() {
    if (_formKey.currentState.validate()) {
      campoUsuario =
          controladorIdUsuario.text; //capturamos lo que ingreso el usuario
      campoPassword = controladorPasswordUsuario.text;
      String id = 'admin';
      String password = 'admin';

      if (campoUsuario == id && campoPassword == password) {
        //comparamos localmente
        verifica = true;
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SegundaPagina()),
        );
      } else {
        verifica = false;
      }
      setState(() {
        msj = verifica.toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget TextFieldIdContenedor = Container(
      margin: const EdgeInsets.only(top: 60),
      alignment: Alignment.center,
      //colocaremos nuestro widget TextFormField dentro del Widget Container para poder dimensionarlo
      padding: const EdgeInsets.symmetric(horizontal: 90, vertical: 0),
      child: TextFormField(
        controller: controladorIdUsuario,
        //conectamos nuestro controlador al TextField
        validator: (value) {
          if (value.isEmpty) {
            return "Ingresa nombre de usuario";
          }
        },
        decoration: InputDecoration(
          icon: Icon(
            Icons.perm_identity, //icono
            color: Colors.red[500],
          ),
          hintText: "Usuario",
        ),
        keyboardType:
            TextInputType.text, //especificamos el tipo de texo a ingresar.
      ),
    );

    Widget TextFieldPasswordContenedor = Container(
      margin: const EdgeInsets.only(top: 10),
      alignment: Alignment.center,
      //colocaremos nuestro widget TextFormField dentro del Widget Container para poder dimensionarlo
      padding: const EdgeInsets.symmetric(horizontal: 90, vertical: 0),
      child: TextFormField(
        controller: controladorPasswordUsuario,
        validator: (value) {
          if (value.isEmpty) {
            return "Ingresa contraseña";
          }
        },
        decoration: InputDecoration(
          icon: Icon(
            Icons.lock, //icono
            color: Colors.red[500],
          ),
          hintText: "Password",
        ),
        keyboardType: TextInputType
            .visiblePassword, //especificamos el tipo de texo a ingresar.
      ),
    );

    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white, //Color background
      //No usaremos el AppBar

      body: Form(
        key: _formKey, // Creamos un widget Form con nuestro formkey
        child: ListView(children: <Widget>[
          Container(
            height: 50, //contenedor para bajar la imagen
          ),
          Image.asset(
            'assets/img/img_inicio.png',
            //previo mapeo en el archivo pubspec.yaml
            height: 200,
            width: 200,
            fit: BoxFit.cover, //para que encaje la imagen
          ),
          TextFieldIdContenedor,
          TextFieldPasswordContenedor,
          Center(
            child: RaisedButton(
              color: Colors.red,
              child: Text('Ingresar', style: TextStyle(color: Colors.white)),
              onPressed: ValidarUsuario,
            ),
          ),
          Center(
            child: RaisedButton(
              child: Text(
                'Limpiar campos',
                style: TextStyle(color: Colors.black),
              ),
//            onPressed: ValidarUsuario,
              onPressed: () {
                controladorIdUsuario
                    .clear(); //con el metodo clear, podemos limpiar el controlador.
                controladorPasswordUsuario.clear();
              },
            ),
          ),
          Text(
            msj,
            style: TextStyle(fontSize: 30),
          ),
        ]),
      ),
    );
  }
}
