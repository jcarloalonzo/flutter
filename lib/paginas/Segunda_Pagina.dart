import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';

class SegundaPagina extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepPurpleAccent[200],
      appBar: AppBar(
        title: Text("Pagina 2"),
        backgroundColor: Colors.black,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center, //para centrarlo
          children: <Widget>[
            FadeIn(
              delay: Duration(milliseconds: 500),
              child: Image.asset(
                'assets/img/jelaf.png',
                width: 250,
                height: 200,
                fit: BoxFit.contain,
              ),
            ),
            ZoomIn(
              child: FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },

                backgroundColor: Colors.white,
//              child: Text('Volver'),
                child: Icon(
                  Icons.keyboard_backspace,
                  color: Colors.deepPurpleAccent,
                  size: 30,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
